from numbers import Number


def switch(smth: Number) -> Number:
    return smth * -1

if __name__ == "__main__":
    cases = (
        (2, -2),
        (10, -10),
        (17, - 17),
    )
    for value, result in cases:
        assert switch(value) == result, f"ERROR! switch({value}) != {result}, got {switch(value)}"
