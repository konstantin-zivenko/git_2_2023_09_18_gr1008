from numbers import Number


def cube(variable: Number) -> Number:
    """ Cube root calculation function """
    return variable ** (1 / 3)
