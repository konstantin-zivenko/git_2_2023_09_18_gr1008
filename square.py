from numbers import Number


def square(x: Number) -> Number:
    return pow(x, 2)


if __name__ == "__main__":
    cases = (
        (2, 4),
        (3, 9),
        (10, 100),
    )

    for x, result in cases:
        assert square(x) == result, f"ERROR! square({x}) != {result}, got {square(x)}"

